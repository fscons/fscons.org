---
title: Normal registration open!
author: FSCONS
---
Normal registration to FSCONS 2017 in Oslo is open (and the pre-registration period has now ended). [Regisiter here](https://www.eventbrite.com/e/fscons-2017-oslo-tickets-37008324896)
