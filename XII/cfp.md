---
layout: default
title: Call for participation
---
# Call for participation <small>FSCONS XII, Oslo,
 November 2019</small>
<br/>
The Free Society Conference and Nordic Summit (FSCONS) is a meeting place for social change, focused on the future of free software and free society. The conference brings together people from a wide range of fields, and merges the technical with the social, seeking both to activate and challenge. Open discussion and brainstorming are as important as the talks given during the conference.

## Christer och Nico
During the past year, two dear friends and co-organisers of FSCONS have sadly passed away. Nico was part of the organising committee in Gothenburg for many years, taking care of the technical side of things during the event. He gave his time and energy to countless other projects for societal change, always willing to contribute with his skills and knowledge where needed. Christer, an anarchist at heart and in practice, participated in FSCONS for many years and helped organising the FSCONS Oslo, always with a positive attitude and social energy. They are both sorely missed. We wish to dedicate the upcoming conference to their memory.

## Invitation
For the FSCONS XII conference we're looking for proposals for talks and workshops on a number of different subjects.
We're looking for proposals that relate to Free Society or Free Software generally, as well as those which are at the crossroads of the two or which take the philosophy of Free Society and Free Software and brings them to a whole new field. To give you a hint of the scope of the conference, here are some topics already planned: 

* FLOSS for video production
* GStreamer
* Embedded systems
* FLOSS for public administration
* Libre Office Online
* FLOSS for libraries and archives
* Digital resistance
* Societal hacking – environmental issues
* Critical perspectives on X {Block chain, Zippie etc}
* Copyright and piracy 2019 and beyond – is piracy dead?
* State of art in free communications


But don't let this restrict your thinking: you can submit a proposal even if it doesn't fit into any of the above tracks. And if you are passionate about any of the topics above, and want to see it realised as an entire track, then please get in touch with us to volunteer as track manager. 

_The deadline for your proposal is May 1 st_.

## How to submit your proposal
In order to submit your proposal, please go to FSCONS XII [https://fscons.org], create an user account and make sure you provide valid contact information.  It is very helpful if you, upon user account creation, fill in a short biography. Next, click on "Contribute" [https://fscons.org/node/add/session] to get to the session proposal form. This proposal form is to be used only for your personal proposal. If you have ideas of other people you want to suggest as speakers, please feel free to suggest those [org/wiki/]

When you fill out the proposal form, we would like to ask you to provide the following information. If you don't want your proposal to be public during the review period, please make sure to uncheck the "Make public" checkbox.

* Title - The title of your session/presentation
* Presenter(s) 
Add the presenters of this session. Your own username is filled in by default, but you can change it. You can add any number of presenters to the session, but everyone that you add must have previously created their own user account on the web site. 

*  Description
The abstract and description of your presentation.

After the review period, ending June 1 st, you will get notified if your proposal is accepted or not.

## Important dates for FSCONS XII
* May 1 st - deadline for submissions
* June 1 st - accepted proposals will be notified. 

If your proposal is accepted, the abstract submission deadline is July 1.

Looking forward to see you at FSCONS XII in Oslo!

_Programme managers, Leif-Jöran and Kacper_
