---
title: FSCONS 2016 and Beyond
author: Andreas Skyman, Stian Rødven Eide
---
Dear participants and friends of FSCONS,

After the annual meeting we are happy to inform you that the planning
for FSCONS 2016 has started (and not only that: we've started looking
ahead to 2017 and 2018 as well, but more on that later). We are aiming
for the second weekend of November as usual, that is to say 12-13
November.

FSCONS 2016 will, as previously suggested, take place at a conference
centre in the vicinity of Gothenburg with a limited number of speakers
and tracks, and tickets for the event being essentially the cost price
for the accommodation at the venue. This, we trust, will not only
alleviate the work load of the organisers and thus increase the
sustainability of the association, but also provide a more intimate
setting for our 10th anniversary celebration. While this decision was
not taken lightly, changing the venue for this year will reduce the
effort of organising volunteers and logistics, and the reduced number
of speakers means that we can use our buffer to cover the speakers'
travel costs, thereby reducing our need for finding sponsors.

Relying on a conference centre will mean that accommodation and meals
will be included for all participants. While we anticipate that the
total cost for participation for those travelling to Gothenburg will
be about the same, unfortunately the price for local participants will
likely be higher. To alleviate this, any additional funds acquired
from sponsors will primarily go to lowering the ticket price.

In addition to the above, we are excited to report that the annual
meeting has asked our collaborators in Oslo to look into the
possibility of hosting FSCONS 2017. We think that this could bring
some new energy and perspectives to the event, and that alternating
between Gothenburg and another city in the Nordic countries every
other year will also give the organisers more time for the tasks that
need to be performed a year or more in advance, such as inviting high
profile speakers and applying for external funding. This might also free
energy and capacity for organising satellite events in Gothenburg and
elsewhere in the years when the main event is out of town, which is
something we have been wanting to do for many years.

We go into the planning of this year's conference with FSCONS 2017 and
2018 already in mind, a fact which we hope you will find as inspiring
as we do. We hope you will follow us on this journey, and as always
you are more than welcome to join us in organising FSCONS 2016, and to
help us make FSCONS 2016 the Ultimate FSCONS experience!

on behalf of the board,
Andreas Skyman (Secretary)
Stian Rødven Eide (Chairman)
