---
title: FOSS-GBG Satellite Event on Friday 11 November
author: Stian Rødven Eide
---
On the Friday before FSCONS, the local Free Software organisation
[FOSS-GBG](http://foss-gbg.se) will host a special satellite event
in central Gothenburg, which should be interesting for most FSCONS
visitors. This event is gratis and there will be talks and free food.
It begins at 16.30 and is estimated to end around 20.00.
In order to estimate the food serving, everyone who considers visiting
are recommended to register beforehand. You can find descriptions 
of the talks and a link to the ticket registration on
[FOSS-GBG's calendar page for the event](http://foss-gbg.se/kalender/foss-gbg-plus-2016/).

