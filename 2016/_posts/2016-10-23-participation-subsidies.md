---
title: Participation fee subsidies
author: Stian Rødven Eide
---
For those who feel that the participation cost of FSCONS X is too high,
our faithful sponsor Friprogramvarusyndikatet has offered to subsidise
a limited number of tickets. If you wish to apply, simply send an e-mail
to info@fscons.org stating your current occupation and whether you need
the full ticket covered or just part of it will suffice.
