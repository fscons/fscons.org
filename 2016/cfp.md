---
layout: default
title: Call for participation
---
# Call for participation <small>FSCONS 2015, Gothenburg, 7&#x2012;8 Nov</small>
<br/>

The Free Society Conference and Nordic Summit (FSCONS) is a meeting place for
social change, focused on the intersection between technology, culture and
society. FSCONS is organised according to the principles laid out in our
[Manifesto](manifesto.html), a living document accepted by the society's
members at the yearly meeting.

The conference brings together people from a wide range of
fields, and merges the technical with the social, seeking both to
activate and challenge. Open discussion and brainstorming are as
important as the talks given during the conference.

## Invitation

We're looking for proposals that relate to Free Software, Free Culture or Free
Society generally, as well as those which are at the crossroads of the three or
which take the philosophy and brings them to a whole new field.  To give you a
hint of the scope of the conference here are some topics already planned:

* Keepers of Culture
* Everyday Crypto
* P2P Society
* Groundbreaking User Experience
* Biohacking

But don't let this restrict your thinking: you can submit a proposal
even if it doesn't quite fit into any of the above topics.

**The dealine for your proposal is July 15th.**

## How to submit your proposal

In order to submit your proposal, please go to
[frab](https://frab.fscons.org/en/fscons2015/cfp/), register your proposal and
make sure your provide valid contact information.

This proposal form is to be used only for your personal proposal.
If you have ideas of other people you want to suggest as speakers, please feel
free to suggest those in the
[FSCONS Wiki](https://wiki.fscons.org/Suggested_Speakers_2015).

After the review period, ending July 31th, you will get notified if
your proposal is accepted or not.

## Important dates for FSCONS 2015

* July 15th &ndash; deadline for submissions
* July 31th &ndash; accepted proposals will be notified.
* 7&#x2012;8 Nov &ndash; FSCONS 2015!

If your proposal is accepted, the abstract submission deadline is September 1st.

Looking forward to seeing you at FSCONS 2015 in November!

_Programme managers, Stian and Leif-Jöran_

