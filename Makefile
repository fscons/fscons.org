PYTEST=py.test

install:
	cabal sandbox init
	cabal install
	npm install less

rebuild:
	cabal configure
	cabal run rebuild

deploy:
	rsync -r --delete _site/ jenkins@fscons.org:/var/www/fscons.org/ \
	  --exclude=meetbot/

test:
	tox -- --junit-xml=_tests/results.xml
