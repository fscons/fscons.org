fscons.org
==============================================================================

This is the static website for fscons. Other, more interactive part (schedule,
tickets, wiki...), are handled elsewhere.

This project uses hakyll but the only thing you need to build it is a
(relatively recent) haskell platform.

(Or 

`curl -sSL https://get.haskellstack.org/ | sh`

to install a standalone GHC not interfering with the system's version.)

    stack setup
    stack build
    ./scripts/bootstrap
    cabal update
    stack exec site build

The generated pages end up in _site/

There is also the option of using hakyll built-in web server which will rebuild
pages when a source file changes (http://localhost:8000):

    stack exec site watch

Features
========

- Every conference can have its own set of templates
- Global rss feed for news posts. (all conferences mixed)

To create a new conference
====================
- Copy a previous conference directory, e.g. 2017 -> XII
- Add the new conference, e.g. XII, in site.hs
- Requires a build `stack build` 
- Edit the new conference
- Follow in browser like above with `stack exec site watch` 

Contribute
==========

- Issue Tracker: https://gitlab.com/fscons/fscons.org/issues
- Source Code: https://gitlab.com/fscons/fscons.org/

Icons
-----

Most of the icons come from http://thenounproject.com

They should be converted to png and resized to 50x32
To create an icon of the expected size with imagemagick (from a svg, probably
other format as well)

    convert -resize x32 -extent 50x -gravity center -background none noun_project_14383.svg ic_schedule.png

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: org@lists.fscons.org
