---
title: Friday Meetup
author: Stian Rødven Eide
---
Dear participants of FSCONS 2015,

Unlike previous years, we have not scheduled an official social
gathering on Friday evening for this year's FSCONS.

Instead, we have booked a big table at a nearby pub, where you all are
invited to come and meet each other. The pub in question is this one:
[http://www.bishopsarms.com/Goteborg___Park_Avenue](http://www.bishopsarms.com/Goteborg___Park_Avenue)

The table is booked from 19.30, as before that many of us will attend
Ken MacLeods other talk, scheduled at the City Library
(Stadsbiblioteket - located 50 meters from the pub) at 18.00.

So, please join us at the library, hang out with us at the pub, or
simply get a long night's sleep before the conference starts on
Saturday morning.

Be seeing you!

The FSCONS Team

