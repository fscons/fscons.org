---
title: Preliminary schedule available
author: Grégoire Détrez
---

The preliminary schedule for FSCONS 2015 is now [available][1].

Check-out our fantastic keynote speakers: [Birgitta Jónsdóttir][2], poetician
and activist in the Icelandic Parliament for the Pirate Party; and [Ken
MacLeod][3], a biologist turned computer programmer turned science fiction
writer. I bet they will feel at home at this year's FSCONS with tracks such as
*P2P Society*, *Biohacking*, *Keepers of Culture*, *Everyday Crypto* and
*Groundbreaking User Experiences*.

What are *you* gonna learn about at this year's FSCONS?

Early-bird tickets are available until the end of September only, register now
on [fscons.org][4]! (Regular tickets will be available up to one week before the
conference.)

[1]: https://fscons.org/2015/schedule/
[2]: https://frab.fscons.org/en/fscons2015/public/speakers/210
[3]: https://frab.fscons.org/en/fscons2015/public/speakers/209
[4]: https://fscons.org/2015/register.html
