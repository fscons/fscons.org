---
title: Post Conference Summary
author: Oliver Propst
--- 

*FSCONS 2015 took place at the Gotuenburg Universtiy venue Humanisten, November 7-8. A summary follows below*

[Birgitta Jónsdóttir][1] (Poet, member of the Icelandic Parliament), and [Ken MacLeod][2] (Scottish science fiction writer) 
delivered Keynotes which encouraged the audience to view society as a system which can be changed, asked questions and 
offered clues on how informed citizens with access to modern technology can make society progress in a democratic way.

![Keynote Speaker Birgitta Jónsdóttir at FSCONS]($baseurl$/images/post2015/keynote-speaker-birgitta-jónsdóttir-at-fscons.jpg)

Presentations on a spectrum of relevant topics were given including peer-to-peer technology, 
encryption and user experience design*. Workshops were hosted about Biohacking and the IoT, internet of things 
space. Even waffle making took place at FSCONS 2015.

Also several prominent Free Software projects and organizations were present including [FSFE] [3], [Debian][4] and [Mozilla] [5].

We are proud of how FSCONS 2015 turned out and want to express a deep Thank You to everyone* who contributed 
to make FSCONS 2015 happen. 


**Photos** 

![General conference manager Leif-Jöran-Olsson engaged in conversations at FSCONS 2015]($baseurl$/images/post2015/general-conference-manager-leif-jöran-olsson-engaged-in-conversations-at-fscons.jpg)
![FSCONS Banner]($baseurl$/images/post2015/fscons-banner.jpg)
![Directions to the fscons Debian room]($baseurl$/images/post2015/directions-to-the-fscons-debian-room.jpg)
![Discussions taking place in FSCONS Biohacking room]($baseurl$/images/post2015/discussions-taking-place-in-fscons-biohacking-room.jpg)
![How to surf the web with TOR presentation at FSCONS]($baseurl$/images/post2015/how-to-surf-the-web-with-tor-presentation-at-fscons.jpg)
![Gustav Ek presentes fripost at fscons]($baseurl$/images/post2015/gustav-ek-presentes-fripost-at-fscons.jpg)
![IoT workshop participants]($baseurl$/images/post2015/iot-workshop-participants.jpg)
![FSCONS table area]($baseurl$/images/post2015/fscons-table-area.jpg)
![Waffle making in progress at FSCONS]($baseurl$/images/post2015/waffle-making-in-progress-at-fscons.jpg)
![Lightning talk audience at FSCONS]($baseurl$/images/post2015/lightning-talk-audience-at-fscons.jpg)


Photos by [Oliver Propst][6] licensed under [CA-BY-SA 4.0] [7]

*Recordings from most sessions are [available][8] on Youtube.

*This includes volunteers, [sponsors and partners][8] whom without FSCONS 2015 would not be possible. 

[1]: https://frab.fscons.org/en/fscons2015/public/speakers/210 "Birgitta Jónsdóttir"
[2]: https://frab.fscons.org/en/fscons2015/public/speakers/209 "Ken MacLeod"
[3]: http://fsfe.org/ "FSFE"
[4]: http://www.debian.org/ "Debian"
[5]: https://www.mozilla.org/en-US/ "Mozilla"
[6]: https://500px.com/oliverpropst
[7]: https://creativecommons.org/licenses/by-sa/4.0/
[8]: https://fscons.org/2015/sponsors.html "Sponsors and Partners"
[9]: https://www.youtube.com/playlist?list=PL4yYmsifqkVGwbVaSXRJh8FTTPsvcVmVS "FSCONS recordings"
